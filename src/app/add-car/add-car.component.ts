import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Car } from '../car';
import { CarService } from '../car.service';

@Component({
  selector: 'app-add-car',
  templateUrl: './add-car.component.html',
  styleUrls: ['./add-car.component.css']
})
export class AddCarComponent implements OnInit {

  car: Car;

  constructor(
    private carService: CarService,
    private location: Location,
    private router: Router
  ) {}
  
  ngOnInit() {
  }

  onSubmit(form) {

    console.log("IN ADD COMPONENT");
    console.log(form.value["plateNumber"])
    const newCar = new Car(

      form.value["plateNumber"],
      form.value["brand"],
      form.value["price"]
    );

  this.carService.addCar(newCar).subscribe((response) => {console.log(response)});
  this.router.navigate(['/cars']);
  }

  goBack(): void {
    this.location.back();
  }

}