import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Car } from '../car';
import { CarService } from '../car.service';

@Component({
  selector: 'my-cars',
  templateUrl: './car.component.html'
})

export class CarComponent implements OnInit {

  title = 'Car Rental';
  cars: Car[];
  selectedCar: Car;
  constructor(
    private carService: CarService,
    private router: Router) { }

  ngOnInit() {
    this.carService.getCars().subscribe(cars => this.cars = cars);
  }


  onSelect(car: Car): void {
    this.selectedCar = car;
  }

  gotoDetail(): void {
    this.router.navigate(['/cars', this.selectedCar.plateNumber]);
  }

}
