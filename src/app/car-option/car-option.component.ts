import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Location } from '@angular/common';
import { Car } from '../car';
import { CarService } from '../car.service';

@Component({
  selector: 'app-car-option',
  templateUrl: './car-option.component.html',
  styleUrls: ['./car-option.component.css']
})
export class CarOptionComponent implements OnInit {

  car: Car;

  constructor(
    private carService: CarService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {}


  ngOnInit() {
    const plateNumber = this.route.snapshot.paramMap.get("plateNumber");
    this.carService.getCar(plateNumber).subscribe(car => this.car = car);
  }
  rent(car): void {
    this.carService.rent(car).subscribe(car => this.car = car);
    this.location.back();
  }

  cancelRental(car): void {
    this.carService.getBack(car).subscribe(car => this.car = car);
    this.router.navigateByUrl("/cars")
  }

  goBack(): void {
    this.location.back();
  }
}


