import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarComponent } from './car/car.component';
import { CarOptionComponent } from './car-option/car-option.component';
import { AddCarComponent } from './add-car/add-car.component';

const routes: Routes = [
  { path: '', redirectTo: '/cars', pathMatch: 'full' },
  { path: 'cars',     component: CarComponent },
  { path: 'cars/:plateNumber', component: CarOptionComponent },
  { path: 'addcars', component: AddCarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: "reload"})],
  exports: [RouterModule]
})
export class AppRoutingModule { }



