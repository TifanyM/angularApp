import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs' 
import { Car } from './car';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  cars: Car[];
  constructor(private http: HttpClient) {}

  getCars(): Observable <Car[]>{
    return this.http.get<Car[]>("https://projetajd3.appspot.com/cars")
  }


  getCar(plateNumber): Observable<Car> {
    return this.http.get<Car>("https://projetajd3.appspot.com/cars/"+plateNumber);
  }
  
  rent(car): Observable<any> {
    return this.http.put("https://projetajd3.appspot.com/rent/"+car.plateNumber+"?rent=true", null);
  }

  getBack(car): Observable<any> {
    return this.http.put("https://projetajd3.appspot.com/rent/"+car.plateNumber+"?rent=false", null);
  }

  addCar(car): Observable<any> {
    return this.http.post("https://projetajd3.appspot.com/car", car);
  }
}





  